# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['Work']


class Work(metaclass=PoolMeta):
    __name__ = 'timesheet.work'

    @classmethod
    def __setup__(cls):
        super(Work, cls).__setup__()
        cls.yield_record_granularity.selection.extend([('team', 'Team')])
